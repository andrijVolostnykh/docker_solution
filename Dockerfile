FROM node:12-alpine

WORKDIR /app

COPY package*.json ./
COPY src ./
COPY public ./

RUN npm install

COPY . .

CMD ["npm", "run", "start"]
EXPOSE 3000
